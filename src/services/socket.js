var socketIo = require("socket.io");
var mongoose = require("mongoose");
var schema = require("../model/schema");

mongoose.connect("mongodb://localhost:27017/ring-the-golden-bell", {
  useNewUrlParser: true
});

const adminStartTimer = require("./sockets/adminSendStartTimer");
const adminSendLevelUp = require("./sockets/adminSendLevelUp");
const adminSendShowAnswer = require("./sockets/adminSendShowAnswer");
const adminSendShowHelp = require("./sockets/adminSendShowHelp");
const adminSendShowOption = require("./sockets/adminSendShowOption");
const adminSendCheckResult = require("./sockets/adminSendCheckResult");
const adminSendRefreshInfo = require("./sockets/adminSendRefreshInfo");
const adminSendChangeLive = require("./sockets/adminSendChangeLive");


const userSendAnswer = require("./sockets/userSendAnswer");



var refsocketids = mongoose.model("refsocketids");
var status = mongoose.model("status");
var questions = mongoose.model("questions");
var players = mongoose.model("players");
var histories = mongoose.model("histories");
var users = mongoose.model("users");

var io = socketIo();

var socketApi = { io };

adminStartTimer(io);
adminSendLevelUp(io);
adminSendShowAnswer(io);
adminSendShowHelp(io);
adminSendShowOption(io);
adminSendCheckResult(io);
adminSendRefreshInfo(io);
adminSendChangeLive(io);
userSendAnswer(io);


io.on("connection", function(socket) {
  console.log("A user connected: " + socket.id);

  socket.on("check", data => {
    console.log(data);
  });

  socket.on("admin-send-letgetinformation", function(data) {
    console.log("admin said: " + data);
    io.sockets.emit(
      "server-send-getinformation",
      "get information about refsockets"
    );
  });

  socket.on("admin-send-showinfoafterget", function(data) {
    // get instances of model refsocketids -> send it to adminpage
    var result;
    refsocketids.find().exec((err, content) => {
      for (var i = 0; i < content.length; i++) {
        result = content[i].name + "," + content[i].socketId;
        socket.emit("server-send-information-refsocketids", result);
      }
    });
  });

  // receive information about model refsocketids
  socket.on("client-send-information", function(data) {
    var content = data.split(",");
    console.log("Client send information: " + content[0] + " " + content[1]);

    // update socketId of client
    refsocketids.remove({ name: content[1] }).exec();
    let index = { name: content[1], socketId: content[0] };
    refsocketids.create(index);
  });

  socket.on("admin-send-RefreshInfoPlayer", function(data) {
    // Get info about players
    var result;
    players.find().exec((err, content) => {
      // create result to send
      result = content[0].playerId + "#" + content[0].live;
      for (let i = 1; i < content.length; i++) {
        result = result + "#" + content[i].playerId + "#" + content[i].live;
      }

      console.log(result);
      let socketidsideled1;
      refsocketids.find({ name: "sideled1" }).exec((err, content) => {
        try {
          socketidsideled1 = content[0].socketId;
        } catch {
          let err =
            "Catch err socketId in socket.on: admin-send-RefreshInfoPlayer";
          console.log(err);
          socket.emit("server-send-error", err);
        }

        // Send Info of players to Led 2 3
        io.to(socketidsideled1).emit("server-send-refreshinfo", result);
      });
    });
  });

  

  socket.on("admin-send-ListPlayerLevelUp", function(data) {
    let content = data.split(",");
    for (let i = 0; i < content.length; i++) {
      players.find({ playerId: content[i] }).exec((err, res) => {
        let valueOfLive;
        valueOfLive = res[0].live + 1;
        players
          .update({ playerId: res[0].playerId }, { live: valueOfLive })
          .exec();
      });
    }
  });

  socket.on("admin-send-ListPlayerLevelDown", function(data) {
    let content = data.split(",");
    for (let i = 0; i < content.length; i++) {
      players.find({ playerId: content[i] }).exec((err, res) => {
        let valueOfLive;
        valueOfLive = res[0].live - 1;
        players
          .update({ playerId: res[0].playerId }, { live: valueOfLive })
          .exec();
      });
    }
  });

  socket.on("admin-send-ShowResult", function(data) {
    status.find().exec((err, content) => {
      let currentquestion = content[0].currentQuestion;

      // Get answer of questionId: currentQuestion
      let done = 0;
      let answer;
      let result = "";
      questions.find({ questionId: currentquestion }).exec((err, content) => {
        answer = content[0].answer;

        // Setup model players about live before update
        for (let i = 1; i <= 30; i++) {
          players.find({ playerId: i }).exec((err, content) => {
            // set value of live
            let valueOfLive = content[0].live;
            let stillAlive = 0;
            if (valueOfLive > 0) {
              valueOfLive = valueOfLive - 1;
              stillAlive = 1;
            }

            // Get result
            histories
              .find({ questionId: currentquestion, playerId: i })
              .exec((err, content) => {
                if (stillAlive === 1) {
                  // case: player have an answer
                  if (content.length > 0) {
                    if (answer === content[0].answerOfclient) {
                      // correct answer
                      valueOfLive = valueOfLive + 1;
                      result = result + i + "#1#";
                    } else {
                      // correct wrong answer
                      result = result + i + "#0#";
                    }
                  } else {
                    // case: no answer
                    result = result + i + "#0#";
                  }
                } else {
                  // player was died
                  result = result + i + "#-1#";
                }

                // update players.live
                players
                  .update({ playerId: i }, { live: valueOfLive })
                  .exec(function() {
                    done = done + 1;
                    if (done === 30) {
                      send(result);
                    }
                  });
              });
          });
        }
      });
    });
  });

  function send(data) {
    let socketidsideled1;
    refsocketids.find({ name: "sideled1" }).exec((err, content) => {
      try {
        socketidsideled1 = content[0].socketId;
      } catch {
        let err = "Catch err socketId in socket.on: admin-send-showResult";
        console.log(err);
        socket.emit("server-send-error", err);
      }
      // Send Option of players to Led 2 3
      io.to(socketidsideled1).emit("server-send-showResult", data);
    });
  }

  socket.on("admin-send-cleardatabase", function(data) {
    // Clear all collections in database ring-the-golden-bell before setup (6 models)
    clearDatabase();
  });

  function clearDatabase() {
    // 1. clear model histories
    histories.deleteMany().exec(() => {
      // 2. clear model players
      players.deleteMany().exec(() => {
        // 3. clear model refsocketids
        refsocketids.deleteMany().exec(() => {
          // 4. clear model status
          status.deleteMany().exec(() => {
            // 5. clear model questions
            questions.deleteMany().exec(() => {
              // 6. clear model users
              users.deleteMany().exec(() => {
                // Send result to the admin at setup page
                socket.emit("server-send-ACKclear", "done");
              });
            });
          });
        });
      });
    });
  }

  socket.on("admin-send-supportPlayers", function(data) {
    let socketidmiddleled;

    refsocketids.find({ name: "middleled" }).exec((err, content) => {
      try {
        socketidmiddleled = content[0].socketId;
      } catch {
        let err =
          "Catch err socketId not defined in socket.on: admin-send-showquestion";
        console.log(err);
        socket.emit("server-send-error", err);
      }

      io.to(socketidmiddleled).emit(
        "server-send-supportPlayers",
        "Support for players"
      );
    });
  });

  socket.on("disconnect", function() {
    console.log("Disconnected: " + socket.id);

    // delete record have "socketId = socket.id"
    refsocketids.remove({ socketId: socket.id }).exec();
  });
});

module.exports = {
  socketApi,
  sendQuestion: (userId, data) => {
    io.emit(`server-send-question_${userId}`, data);
  },
  startTimer: userId => {
    io.emit(`server-start-Timer_${userId}`, "SERVER_START_TIMER");
  }
};
