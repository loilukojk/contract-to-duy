const mongoose = require("mongoose");
const Users = mongoose.model("users");
const Histories = mongoose.model("histories");
const Questions = mongoose.model("questions");

const adminStartTimer = io => {
  io.sockets.on("connection", function(socket) {
    socket.on("admin-send-starttimer", async function(data) {
      console.log("starttimer");
      // send signal to all phoneplayer
      // send signal to middle led
      const users = await Users.find();
      users.map(user => {
        io.emit(`server-start-Timer_${user._id}`, "Start Timer");
        // start timer
        setTimeout(async () => {
          console.log("time's up");
          // Time'up =>> Now! we emit "waiting screen to all phoneplayer"
          const question = await Questions.findOne({ selected: true });
          if (question) {
            const history = await Histories.findOne({
              questionId: question._id,
              userId: user._id
            });
            console.log(history);

            if (
              !history &&
              ["admin", "sideled1", "sideled2", "middleled"].indexOf(
                user.username
              ) === -1
            ) {
              const defaultHistory = new Histories({
                userId: user._id,
                answerOfClient: "none",
                isCorrectAnswer: false,
                questionId: question._id
              });

              user.live = user.live - 1;
              await user.save();
              await defaultHistory.save();
            }
          }
          io.emit(`server-stop-Timer_${user._id}`, "Start Timer");
        }, 5000);
      });
    });
  });
};

module.exports = adminStartTimer;
