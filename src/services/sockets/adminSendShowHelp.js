const mongoose = require("mongoose");
const Users = mongoose.model("users");

const adminSendShowHelp = io => {
  io.sockets.on("connection", function(socket) {
    socket.on("admin-send-show-help", async () => {
      const user = await Users.findOne({ username: "middleled" });
      console.log(user);
      if (user) {
        io.emit(`server-send-show-help_${user._id}`, "SERVER_SEND_SHOW_HELP");
      }
    });
  });
};

module.exports = adminSendShowHelp;
