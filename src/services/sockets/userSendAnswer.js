const mongoose = require("mongoose");
const Users = mongoose.model("users");
const Questions = mongoose.model("questions");
const Histories = mongoose.model("histories");

const userSendAnswer = io => {
  io.sockets.on("connection", function(socket) {
    socket.on("user-send-answer", async payload => {
      try {
        const data = JSON.parse(payload);
        const question = await Questions.findOne({ selected: true });
        const prevHistory = await Histories.findOne({
          userId: data.userId,
          questionId: question._id
        });
        if (!prevHistory) {
          const user = await Users.findById(data.userId);

          const history = new Histories({
            userId: data.userId,
            answerOfClient: data.answer,
            isCorrectAnswer: data.answer === question.answer,
            questionId: question._id
          });

          if (user) {
            user.live = user.live + (history.isCorrectAnswer ? 0 : -1);
            await user.save();
          }
          await history.save();
        } else {
          socket.emit(
            `server-send-denied-submit`,
            "You have aldready submitted!"
          );
        }
      } catch (error) {}
    });
  });
};

module.exports = userSendAnswer;
