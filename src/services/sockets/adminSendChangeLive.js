const mongoose = require("mongoose");
const Users = mongoose.model("users");

module.exports = io => {
  io.sockets.on("connection", function(socket) {
    socket.on("admin-send-change-live", async payload => {
      try {
        const data = JSON.parse(payload);
        const user = await Users.findById(data.userId);
        user.live = user.live + (data.isIncrease ? 1 : -1);
        console.log(user);
        await user.save();
      } catch (error) {
        console.log(error);
      }
    });
  });
};
