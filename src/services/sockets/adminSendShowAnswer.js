const mongoose = require("mongoose");
const Users = mongoose.model("users");
const Questions = mongoose.model("questions");

const adminSendShowAnswer = io => {
  io.sockets.on("connection", function(socket) {
    socket.on("admin-send-showanswer", async id => {
      const user = await Users.findOne({ username: "middleled" });
      console.log(user)
      if (user) {
        const question = await Questions.findById(id);
        if (question) {
          io.emit(`server-send-show-answer_${user._id}`, question.answer);
        }
      }
    });
  });
};

module.exports = adminSendShowAnswer;

//     // get currentQuestion
//     var currentquestion;
//     var result;
//     status.find().exec((err, content) => {
//       currentquestion = content[0].currentQuestion;

//       // get answer
//       if (currentquestion > -2) {
//         questions
//           .find({ questionId: currentquestion })
//           .exec((err, content) => {
//             result = content[0].answer;
//             console.log("result: " + result);
//             // send to middleled
//             io.to(socketidmiddleled).emit("server-send-showanswer", result);
//           });
//       } else {
//         result = "không có đáp án";
//         console.log("result: " + result);
//       }
//     });
//   });
// });
