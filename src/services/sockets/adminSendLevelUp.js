const mongoose = require("mongoose");
const Users = mongoose.model("users");

const adminSendLevelUp = io => {
  io.sockets.on("connection", function(socket) {
    socket.on("admin-send-levelUpCondition", async function(data) {
      const users = await Users.find();
      users.map(async user => {
          if(user.live > 0 ){
              user.live = user.live + 1;
              await user.save();
          }
      })
    });
  });
};

module.exports = adminSendLevelUp;
