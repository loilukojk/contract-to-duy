const mongoose = require("mongoose");
const Users = mongoose.model("users");
const Questions = mongoose.model("questions");
const Histories = mongoose.model("histories");

const adimSendCheckResult = io => {
  io.sockets.on("connection", function(socket) {
    socket.on("admin-send-check-result", async () => {
      const user = await Users.findOne({ username: "sideled1" });
      if (user) {
        const question = await Questions.findOne({ selected: true });
        if (question) {
          const histories = await Histories.find({
            questionId: question._id
          }).populate({ path: 'userId', select: 'username' });
          io.emit('server-send-check-result', JSON.stringify(histories))
        }
      }
    });
  });
}

module.exports = adimSendCheckResult;
