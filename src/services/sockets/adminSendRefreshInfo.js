const mongoose = require("mongoose");
const Users = mongoose.model("users");

module.exports = io => {
  io.sockets.on("connection", function(socket) {
    socket.on("admin-send-refresh-info", async () => {
      const users = await Users.find({
        username: { $nin: ["admin", "sideled1", "sideled2", "middleled"] }
      }).select("username live");
      io.emit("server-send-refresh-info", JSON.stringify(users));
    });
  });
};
