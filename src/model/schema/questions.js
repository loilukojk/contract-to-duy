var mongoose = require('mongoose');

var questions = new mongoose.Schema({
  questionId: {
    required: true,
    type: Number,
  },
  name: {
    required: true,
    type: String,
  },
  content: {
      required: true,
      type: String,
  },
  type: {
      required: true,
      type: String,
  },
  optionA: {
      required: false,
      type: String,
  },
  optionB: {
    required: false,
    type: String,
  },
  optionC: {
    required: false,
    type: String,
  },
  optionD: {
    required: false,
    type: String,
  },
  image: {
    required: false,
    type: String,
  },
  linkaudio: {
      required: false,
      type: String,
  },
  hint: {
      required: false,
      type: String,
  },
  answer: {
      required: true,
      type: String
  },
  selected: {
    type: Boolean,
    default: false
  }  
})

module.exports = questions;