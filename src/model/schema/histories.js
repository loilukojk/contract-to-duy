var mongoose = require("mongoose");

var histories = new mongoose.Schema({
  userId: {
    required: true,
    type: mongoose.Types.ObjectId,
    ref: "users"
  },
  questionId: {
    required: true,
    type: mongoose.Types.ObjectId,
    ref: "questions"
  },
  answerOfClient: {
    required: true,
    type: String
  },
  isCorrectAnswer: {
    required: true,
    type: Boolean,
    default: false
  }
});

module.exports = histories;
