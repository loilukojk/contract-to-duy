var mongoose = require("mongoose");

var users = new mongoose.Schema({
  username: {
    required: true,
    type: String
  },
  password: {
    required: true,
    type: String
  },
  roles: [
    {
      type: String
    }
  ],
  fullname: {
    required: true,
    type: String
  },
  live: {
    required: true,
    type: Number
  },
  teamNumber: {
    required: true,
    type: Number,
    default: 0,
  }
});

module.exports = users;
