module.exports = {
  users: require("./users"),
  questions: require("./questions"),
  players: require("./players"),
  histories: require("./histories"),
  refsocketids: require("./refsocketids"),
  status: require("./status")
};
