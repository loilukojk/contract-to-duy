var mongoose = require('mongoose');

var refsocketids = new mongoose.Schema({
  name: {
    required: true,
    type: String,
  },
  socketId: {
      required: true,
      type: String,
  }
})

module.exports = refsocketids;