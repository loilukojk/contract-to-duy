var mongoose = require('mongoose');

var players = new mongoose.Schema({
  playerId: {
    required: true,
    type: Number,
  },
  password: {
      required: true,
      type: String,
  },
  live: {
      required: true,
      type: Number,
  },
  idOnline: {
      required: true,
      type: Boolean
  },
  socketId: {
      required: false,
      type: Object
  }
})

module.exports = players;