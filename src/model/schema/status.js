var mongoose = require('mongoose');

var status = new mongoose.Schema({
  currentQuestion: {
    required: true,
    type: Number,
    default: -2 
  }
})

module.exports = status;