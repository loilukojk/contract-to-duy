var mongoose = require('mongoose');
var schema = require('./schema/index');

module.exports = {
  users: mongoose.model('users', schema.users),
  questions: mongoose.model('questions', schema.questions),
  players: mongoose.model('players', schema.players),
  histories: mongoose.model('histories', schema.histories),
  refsocketids: mongoose.model('refsocketids', schema.refsocketids),
  status: mongoose.model('status',schema.status)
}
