var router = require("express").Router();
var passport = require("passport");

//dang nhap
router.get("/", async function(req, res, next) {
  res.render("adminpage/users/login");
});

router.post(
  "/",
  passport.authenticate("local", {
    //kiểm tra quyền truy cập xem có đăng nhập lần nào chưa
    failureRedirect: "/dang-nhap", //trả về trang /dang-nhap nếu chưa có quyền
    failureFlash: false
  }),
  (req, res) => {
    if (req.body.remember) {
      //đăng nhập thành công sẽ tạo cookie lưu trữ thông trong 30 ngày
      req.session.cookie.maxAge = new Date(
        Date.now() + 30 * 24 * 60 * 60 * 1000
      ); // Cookie expires after 30 days
    } else {
      req.session.cookie.expires = false; // Cookie expires at end of session
    }
    if (req.user.roles[0] == "USERS") {
      if (req.user.username === "sideled1") {
        return res.redirect("/player-info");
      }
      if (req.user.username === "sideled2") {
        return res.redirect("/play");
      }
      if (req.user.username === "middleled") {
        return res.redirect("/middleled");
      }
      return res.redirect("/"); //chuyển người dùng về trang đăng nhập nếu thành công
    } else {
      return res.redirect("/admin");
    }
  }
);
module.exports = router;
