var { checkPermission } = require("services/checkPermission");
var { IS_ADMIN, IS_USER } = require("config/constants");

//checkPermission để kiểm tra xem người truy cập vào đường dẫn bắt đầu bằng /admin có quyền truy cập hay không
//Nếu không có quyền truy cập hàm  checkPermission sẽ đẩy người dùng về trang đăng nhập
//Sau khi đăng nhập passport sẽ lưu cookie của người dùng đó để xác nhận truy cập /admin cho những lần tiếp theo
//folder login sẽ sử lý việc đăng nhập của người dùng
module.exports = router => {
  router.use("/dang-nhap", require("./login"));
  router.use("/setup", require("./setup"));
  router.use("/admin", checkPermission(IS_ADMIN), require("./adminpage"));
  router.use("/", checkPermission(IS_USER), require("./homepage"));
};
