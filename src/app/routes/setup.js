var router = require("express").Router();
var mongoose = require("mongoose");
var bcrypt = require("bcryptjs");
var { success } = require("../../services/returnToUser");
const Questions = mongoose.model("questions");
mongoose.connect("mongodb://localhost:27017/ring-the-golden-bell", {
  useNewUrlParser: true
});

router.get("/", async (req, res, next) => {
  try {
    let insert = {
      username: "admin",
      password: "123",
      fullname: "Admin",
      live: 1000,
      roles: ["ADMIN"]
    };
    const saltRounds = 10;
    bcrypt.hash(insert.password, saltRounds, async (err, hash) => {
      insert.password = hash;
      let usersInfo = await mongoose.model("users").create(insert);
      console.log(usersInfo);
    });

    // insert admin middleled
    let insertmiddleled = {
      username: "middleled",
      password: "123",
      fullname: "Admin",
      live: 1000,
      roles: ["USERS"]
    };
    bcrypt.hash(insertmiddleled.password, saltRounds, async (err, hash) => {
      insertmiddleled.password = hash;
      let usersInfo = await mongoose.model("users").create(insertmiddleled);
      console.log(usersInfo);
    });
    // insert admin sideled1
    let insertsideled1 = {
      username: "sideled1",
      password: "123",
      fullname: "Admin",
      live: 1000,
      roles: ["USERS"]
    };

    bcrypt.hash(insertsideled1.password, saltRounds, async (err, hash) => {
      insertsideled1.password = hash;
      let usersInfo = await mongoose.model("users").create(insertsideled1);
      console.log(usersInfo);
    });

    let insertsideled2 = {
      username: "sideled2",
      password: "123",
      fullname: "Admin",
      live: 1000,
      roles: ["USERS"]
    };

    bcrypt.hash(insertsideled2.password, saltRounds, async (err, hash) => {
      insertsideled2.password = hash;
      let usersInfo = await mongoose.model("users").create(insertsideled2);
      console.log(usersInfo);
    });

    for (let i = 1; i <= 30; i++) {
      let insertUsers = {
        username: `user_${i}`,
        password: `user_${i}`,
        fullname: `Đội chơi ${i}`,
        teamNumber: i,
        live: 3,
        roles: ["USERS"]
      };
      const saltRounds = 10;
      bcrypt.hash(insertUsers.password, saltRounds, async (err, hash) => {
        insertUsers.password = hash;
        let usersInfo = await mongoose.model("users").create(insertUsers);
        console.log(usersInfo);
      });
    }
    var questions = [
      {
        questionId: -1,
        name: "Câu hỏi demo 1",
        content:
          "Chương trình Sinh viên với biên giới, biển đảo 2019 chủ đề “Tự hào truyền thống biên phòng Việt Nam” do Hội sinh viên cụm 3 tổ chức tại trường nào?",
        type: "tracnghiem",
        optionA: "Đại học Sư phạm Kỹ thuật TP. HCM",
        optionB: "Đại học Ngân hàng TP. HCM",
        optionC: "Cao đẳng Công nghệ Thủ Đức",
        optionD: "Cao đẳng Xây dựng TP. HCM",
        image: "<blank>",
        linkaudio: "<blank>",
        hint: "<blank>",
        answer: "A"
      },
      {
        questionId: 0,
        name: "Câu hỏi demo 2",
        content:
          "Chương trình Sinh viên với biên giới, biển đảo 2019 chủ đề “Tự hào truyền thống …  ... Việt Nam” do Hội sinh viên cụm 3 tổ chức tại trường Đại học Sư phạm Kỹ Thuật TP. HCM",
        type: "tuluan",
        optionA: "<blank>",
        optionB: "<blank>",
        optionC: "<blank>",
        optionD: "<blank>",
        image: "<blank>",
        linkaudio: "<blank>",
        hint: "Đáp án gồm có hai tiếng",
        answer: "BIÊN PHÒNG"
      },
      {
        questionId: 1,
        name: "Câu hỏi 1",
        content:
          "Việt Nam có đường biên giới giáp với những quốc gia, vùng lãnh thổ nào?",
        type: "tracnghiem",
        optionA: "Trung Quốc, Lào, Campuchia",
        optionB: "Trung Quốc, Lào, Thái Lan",
        optionC: "Trung Quốc, Campuchia, Thái Lan",
        optionD: "Lào, Campuchia, Thái Lan",
        image: "<blank>",
        linkaudio: "<blank>",
        hint: "<blank>",
        answer: "A"
      },
      {
        questionId: 2,
        name: "Câu hỏi 2",
        content:
          "Cửa khẩu Quốc tế Mộc Bài thuộc địa phận tỉnh nào của nước ta?",
        type: "tracnghiem",
        optionA: "Bình Phước",
        optionB: "Bình Dương",
        optionC: "Lâm Đồng",
        optionD: "Tây Ninh",
        image: "<blank>",
        linkaudio: "<blank>",
        hint: "<blank>",
        answer: "D"
      },
      {
        questionId: 3,
        name: "Câu hỏi 3",
        content:
          "Năm 2019 là kỷ niệm bao nhiêu năm ngày Truyền thống Bộ đội biên phòng và Ngày Biên phòng toàn dân?",
        type: "tracnghiem",
        optionA: "60, 30",
        optionB: "30, 60",
        optionC: "30, 35",
        optionD: "35, 30",
        image: "<blank>",
        linkaudio: "<blank>",
        hint: "<blank>",
        answer: "A"
      },
      {
        questionId: 4,
        name: "Câu hỏi 4",
        content: "Chiến dịch Biên giới Thu – Đông thắng lợi vào năm nào?",
        type: "tracnghiem",
        optionA: "1949",
        optionB: "1950",
        optionC: "1951",
        optionD: "1952",
        image: "<blank>",
        linkaudio: "<blank>",
        hint: "<blank>",
        answer: "B"
      },
      {
        questionId: 5,
        name: "Câu hỏi 5",
        content:
          "Xin mời các bạn nghe một đoạn trong bài hát Bài ca người lính biên phòng và điền vào từ còn trống trong câu sau đây: “Đồn là nhà, … là quê hương”",
        type: "tracnghiem",
        optionA: "Biển đảo",
        optionB: "Tổ quốc",
        optionC: "Căn cứ",
        optionD: "Biên giới",
        image: "/adminpage/middleledpage/img/question5.jpg",
        linkaudio: "/adminpage/middleledpage/audio/question5.mp3",
        hint: "<blank>",
        answer: "D"
      },
      {
        questionId: 6,
        name: "Câu hỏi 6",
        content: "Hãy sắp xếp các khái niệm sau đây theo thứ tự hợp lý",
        type: "tracnghiem",
        optionA: "Đường cơ sở, Nội thủy, Thềm lục địa, Vùng đặc quyền kinh tế",
        optionB:
          "Nội thủy, lãnh hải, vùng tiếp giáp lãnh hải, thềm lục địa, vùng đặc quyền kinh tế",
        optionC:
          "Nội thủy, lãnh hải, vùng tiếp giáp lãnh hải, vùng đặc quyền kinh tế, thềm lục địa",
        optionD:
          "Thềm lục địa, nội thủy, lãnh hải, vùng tiếp giáp lãnh hải, vùng đặc quyền kinh tế",
        image: "<blank>",
        linkaudio: "<blank>",
        hint: "<blank>",
        answer: "C"
      },
      {
        questionId: 7,
        name: "Câu hỏi 7",
        content:
          "Theo Luật Biên giới quốc gia 2003, việc xuất cảnh, nhập cảnh, quá cảnh, xuất khẩu, nhập khẩu qua biên giới quốc gia được thực hiện tại đâu?",
        type: "tracnghiem",
        optionA: "Tại đồn biên phòng nơi tiếp giáp giữa lãnh thổ hai quốc gia",
        optionB: "Tại cửa khẩu",
        optionC: "Tại các bến cảng",
        optionD: "Tất cả đều sai",
        image: "<blank>",
        linkaudio: "<blank>",
        hint: "<blank>",
        answer: "B"
      },
      {
        questionId: 8,
        name: "Câu hỏi 8",
        content: "Bộ Tư lệnh Bộ đội biên phòng trực thuộc?",
        type: "tracnghiem",
        optionA: "Bộ Quốc phòng",
        optionB: "Bộ Công an",
        optionC: "Chính phủ",
        optionD: "Quốc hội",
        image: "<blank>",
        linkaudio: "<blank>",
        hint: "<blank>",
        answer: "A"
      },
      {
        questionId: 9,
        name: "Câu hỏi 9",
        content:
          "Lãnh hải của Việt Nam rộng bao nhiều hải lý tính từ đường cơ sở ra phía ngoài?",
        type: "tracnghiem",
        optionA: "Mười hai",
        optionB: "Mười ba",
        optionC: "Mười bốn",
        optionD: "Mười sáu",
        image: "<blank>",
        linkaudio: "<blank>",
        hint: "<blank>",
        answer: "A"
      },
      {
        questionId: 10,
        name: "Câu hỏi 10",
        content: "Bộ trưởng Bộ Quốc phòng hiện nay là ai?",
        type: "tracnghiem",
        optionA: "Đại tướng Đỗ Bá Tỵ",
        optionB: "Thượng tướng Lương Cường",
        optionC: "Đại tướng Ngô Xuân Lịch",
        optionD: "Thượng tướng Trương Quang Khánh",
        image: "<blank>",
        linkaudio: "<blank>",
        hint: "<blank>",
        answer: "C"
      },
      {
        questionId: 11,
        name: "Câu hỏi 11",
        content: "Cuộc chiến tranh biên giới Tây Nam diễn ra vào năm nào?",
        type: "tracnghiem",
        optionA: "1976",
        optionB: "1979",
        optionC: "1981",
        optionD: "1989",
        image: "<blank>",
        linkaudio: "<blank>",
        hint: "<blank>",
        answer: "B"
      },
      {
        questionId: 12,
        name: "Câu hỏi 12",
        content: "Luật Biên giới quốc gia được Quốc hội ban hành vào năm nào?",
        type: "tracnghiem",
        optionA: "2001",
        optionB: "2002",
        optionC: "2003",
        optionD: "2004",
        image: "<blank>",
        linkaudio: "<blank>",
        hint: "<blank>",
        answer: "C"
      },
      {
        questionId: 13,
        name: "Câu hỏi 13",
        content:
          "Xin mời các bạn quan sát hình ảnh sau và trả lời câu hỏi: Đây là lược đồ mô tả diễn biến của Chiến dịch nào?",
        type: "tracnghiem",
        optionA: "Chiến dịch Biên giới Thu – Đông 1950",
        optionB: "Chiến dịch Việt Bắc 1947",
        optionC: "Chiến dịch Điện Biên Phủ 1954",
        optionD: "Chiến dịch Điện Biên Phủ trên không 1972",
        image: "/adminpage/middleledpage/img/question13.png",
        linkaudio: "<blank>",
        hint: "<blank>",
        answer: "A"
      },
      {
        questionId: 14,
        name: "Câu hỏi 14",
        content:
          "Điền vào chỗ trống trong câu nói sau đây: “Dễ trăm lần không dân cũng chịu, khó vạn lần dân liệu…” ",
        type: "tuluan",
        optionA: "<blank>",
        optionB: "<blank>",
        optionC: "<blank>",
        optionD: "<blank>",
        image: "<blank>",
        linkaudio: "<blank>",
        hint: "Đáp án gồm có hai tiếng",
        answer: "CŨNG XONG"
      },
      {
        questionId: 15,
        name: "Câu hỏi 15",
        content:
          "Bốn cực Đông - Tây - Nam - Bắc của nước ta nằm lần lượt ở bốn tỉnh là?",
        type: "tracnghiem",
        optionA: "Phú Yên - Cà Mau - Hà Giang - Điện Biên",
        optionB: "Phú Yên - Hà Giang - Điện Biên - Cà Mau",
        optionC: "Hà Giang - Cà Mau - Phú Yên - Điện Biên",
        optionD: "Phú Yên - Điện Biên - Cà Mau - Hà Giang",
        image: "<blank>",
        linkaudio: "<blank>",
        hint: "<blank>",
        answer: "D"
      },
      {
        questionId: 16,
        name: "Câu hỏi 16",
        content: "Đâu là tập hợp tên của các tỉnh biên giới phía Bắc nước ta?",
        type: "tracnghiem",
        optionA: "Hà Giang, Hà Nội, Hà Tĩnh",
        optionB: "Hà Giang, Cao Bằng, Lạng Sơn",
        optionC: "Cao Bằng, Hà Nội, Yên Bái",
        optionD: "Cao Bằng, Yên Bái, Bắc Cạn",
        image: "<blank>",
        linkaudio: "<blank>",
        hint: "<blank>",
        answer: "B"
      },
      {
        questionId: 17,
        name: "Câu hỏi 17",
        content:
          "Điền vào chỗ trống các từ còn thiếu trong đoạn thơ quen thuộc sau đây:",
        type: "tuluan",
        optionA: "<blank>",
        optionB: "<blank>",
        optionC: "<blank>",
        optionD: "<blank>",
        image: "/adminpage/middleledpage/img/question17.png",
        linkaudio: "<blank>",
        hint: "<blank>",
        answer: "TÂY TIẾN"
      },
      {
        questionId: 18,
        name: "Câu hỏi 18",
        content: "Đoạn thơ trên được trích từ tác phẩm nào, của tác giả nào?",
        type: "tracnghiem",
        optionA: "Tây Tiến, Lưu Quang Vũ",
        optionB: "Tây Tiến, Tạ Quang Thắng",
        optionC: "Tây Tiến, Quang Dũng",
        optionD: "Đáp án khác",
        image: "<blank>",
        linkaudio: "<blank>",
        hint: "<blank>",
        answer: "C"
      },
      {
        questionId: 19,
        name: "Câu hỏi 19",
        content:
          "Ngã ba biên giới Việt - Lào - Campuchia hay còn gọi là Ngã ba Đông dương tọa lạc tại đâu?",
        type: "tracnghiem",
        optionA: "Xã Bờ Y - huyện Ngọc hồi - tỉnh Kon Tum",
        optionB: "Xã Sín Thầu - huyện Mường Nhé - tỉnh Điện Biên",
        optionC: "Xã Đất Mũi - huyện Ngọc Hiển - tỉnh Cà Mau",
        optionD: "Tất cả đều sai",
        image: "<blank>",
        linkaudio: "<blank>",
        hint: "<blank>",
        answer: "A"
      },
      {
        questionId: 20,
        name: "Câu hỏi 20",
        content:
          "Theo quy định của Luật Biên giới quốc gia năm 2003, Khu vực biên giới bao gồm:",
        type: "tracnghiem",
        optionA: "Khu vực biên giới trên thủy, khu vực biên giới trên bộ",
        optionB: "Khu vực biên giới đất liền, khu vực biên giới hải đảo",
        optionC: "Khu vực biên giới mặt đất, khu vực biên giới trên không",
        optionD: "Khu vực  biên giới trên đất liền, trên biển, trên không",
        image: "<blank>",
        linkaudio: "<blank>",
        hint: "<blank>",
        answer: "D"
      }
    ];
    questions.map(async item => {
      console.log(item)
      let question = new Questions({ ...item });
      await question.save();
    });
    return success(res, "Done");
  } catch (err) {
    console.log(err);
    next(err);
  }
});
module.exports = router;
