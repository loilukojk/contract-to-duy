var router = require('express').Router();

router.get('/middleled', function(req, res, next) {
  res.render('homepage/index', { id: req.user._id });
});

router.get('/', function(req, res, next) {
  res.render('homepage/phoneplayer', { id: req.user._id });
})

router.get('/player-info', function(req, res, next) {
  res.render('homepage/player-info');
});

module.exports = router;
