var mongoose = require("mongoose");
const Questions = mongoose.model("questions");
const Users = mongoose.model("users");
var { sendQuestion } = require("services/socket");
module.exports = {
  sendQuestionToUsers: async questionId => {
    let user = await Users.findOne({ username: "middleled" });
    await Questions.updateMany({}, { selected: false });

    let question = await mongoose.model("questions").findById(questionId);
    question.selected = true;
    await question.save();
    sendQuestion(user._id, question);
  }
};
