var mongoose = require('mongoose');
var { startTimer } = require('services/socket');
module.exports = {
    sendStartTimer: async () => {
        let users = await mongoose.model('users').find();
        users.map(async item => {
            if (item.live > 0) {
                startTimer(item._id)
            }
        })
    }
}