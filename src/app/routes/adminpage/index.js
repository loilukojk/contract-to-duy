var router = require('express').Router();
var mongoose = require('mongoose');
var { sendQuestion } = require('services/socket');
var { sendQuestionToUsers } = require('./users/senQuestion');
var { sendStartTimer } = require('./users/sendStartTimer');

router.get('/', function(req, res, next) {
  res.render('adminpage');
})

router.get('/demo', async (req, res, next) => {
  let users = await mongoose.model('users').find();
  users.map(item => {
    sendQuestion(item._id, { a: "a"})
  })
})


router.get('/middleled', function(req, res, next) {
  res.render('adminpage/middleledpage');
});

// router.get('/sideled1', function(req, res, next) {
//   res.render('adminpage/sideled1');
// });

router.get('/setupdatabase', function(req, res, next) {
  res.render('adminpage/setupdatabase');
});

router.get('/questions', async (req, res, next) => {
  let questions = await mongoose.model('questions').find();
  return res.render('adminpage/questions', { questions });
})

router.get("/players", async (req, res, next) => {
  let users = await mongoose.model('users').find().sort("teamNumber");
  return res.render('adminpage/players', { players: users });
})

router.post('/questions/:id', async (req, res, next) => {
  sendQuestionToUsers(req.params.id)
})

router.post('/start-timer', async (req, res, next) => {
  sendStartTimer()
})

router.use('/users', require('./users'))
router.use('/middleledpage', require('./middleledpage'))

module.exports = router;
