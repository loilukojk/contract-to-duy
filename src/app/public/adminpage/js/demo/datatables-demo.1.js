// Call the dataTables jQuery plugin
$(document).ready(function () {
  $("#dataTable").DataTable();
});

$(document).ready(function(){

  //rows data
  var dataUsers = [
    {
      id: "1",
      lives: "3",
    },
    {
      id: "2",
      lives: "3",
    },
    {
      id: "3",
      lives: "3",
    }
    ,
    {
      id: "4",
      lives: "3",
    },
    {
      id: "5",
      lives: "3",
    },
    {
      id: "6",
      lives: "3",
    }
  ];

  var random_id = function(){
    var id_num = Math.random().toString(9).substr(2,3);
    var id_str = Math.random().toString(36).substr(2);

    return id_num+ id_str;
  }

    //-->create data table > start
    var tbl='';
  
    tbl+='<table class="table table-hover" id="dataTable" width="100%" cellspacing="0">'

          //-->create table header > start
          tbl+='<thead>';
              tbl+= '<tr>'
                tbl+= '<th class="text-center">ID</th>'
                tbl+= '<th class="text-center">Lives</th>'
                tbl+= '<th class="text-center">Actions</th>'
              tbl+= '</tr>'
          tbl+='</thead>';
          //-->create table header > end

          //-->create table body > start
          tbl+='<tbody';

                //-->create table rows > start
                var row_id = random_id();
                $.each(dataUsers,function(index,val){

                      tbl += '<tr  row_id ="'+row_id+'">';
                          tbl+='<td class ="text-center"><div class= "row_data" edit_type="click" col_name="id">'+ val['id']+'</div></td>';
                          tbl+='<td class ="text-center"><div class= "row_data" edit_type="click" col_name="lives">'+ val['lives']+'</div></td>';
                      
                          //-->edit option > start
                          tbl +='<td class ="text-center">';
                              tbl +='<span class="btn_edit" > <a href="#" class="btn btn-link " row_id="'+row_id+'" > Edit</a> </span>'

                              //only show this button if edit button is clicked
                              tbl +='<span class="btn_save"> <a href="#" class="btn btn-link"  row_id="'+row_id+'"> Save</a> | </span>';
                              tbl +='<span class="btn_cancel"> <a href="#" class="btn btn-link" row_id="'+row_id+'"> Cancel</a> | </span>';

                              //tbl+='<button type="button" class="btn btn-xs btn-primary" row_id="'+row_id+'" ><i class="fa fa-plus-square" aria-hidden="true"></i></button>'
                              //tbl+='<span class="btn_plus"> <a href ="#" class ="btn btn_link" row_id="'+row_id+'">Plus</a> | </span>';
                              //tbl+='<span class="btn_sub"> <a href ="#" class ="btn btn_link" row_id="'+row_id+'">Sub</a></span>';
                          tbl +='</td>';
                          //-->edit option > end

                      tbl+= '</tr>';
                });
                //-->create table rows > end

          tbl+='</tbody';
          //-->create table body > end

      tbl+='</table>';
    //-->create data table > end

    //out put table data
    $(document).find('.tbl_user_data').html(tbl);
    
    $(document).find('.btn_save').hide();
    $(document).find('.btn_cancel').hide();    

    //-->make div editable > start
    $(document).on('click','.row_data',function(event){
        event.preventDefault();

        if($(this).attr('edit_type') =='button')
        {
          return false;
        }

        //make div editable
        $(this).closest('div').attr('contenteditable','true');
        //add bg csss
        $(this).addClass('bg-warning').css('padding','5px');

        $(this).focus();
    });           
    //-->make div editable > end
    
        
    //--->save whole row entery > start	
    $(document).on('click', '.btn_save', function(event) 
    {
      event.preventDefault();
      var tbl_row = $(this).closest('tr');

      var row_id = tbl_row.attr('row_id');

      
      //hide save and cacel buttons
      tbl_row.find('.btn_save').hide();
      tbl_row.find('.btn_cancel').hide();

      //show edit button
      tbl_row.find('.btn_edit').show();


      //make the whole row editable
      tbl_row.find('.row_data')
      .attr('edit_type', 'click')	
      .removeClass('bg-warning')
      .css('padding','') 

      //--->get row data > start
      var arr = {}; 
      tbl_row.find('.row_data').each(function(index, val) 
      {   
        var col_name = $(this).attr('col_name');  
        var col_val  =  $(this).html();
        arr[col_name] = col_val;
      });
      //--->get row data > end

      //use the "arr"	object for your ajax call
      $.extend(arr, {row_id:row_id});
      
      //out put to show
      $('.post_msg').html( '<pre class="bg-success">'+JSON.stringify(arr, null, 2) +'</pre>')
      

    });
    //--->save whole row entery > end

    //--->button > edit > start	
    $(document).on('click', '.btn_edit', function(event) 
    {
      event.preventDefault();
      var tbl_row = $(this).closest('tr');

      var row_id = tbl_row.attr('row_id');

      tbl_row.find('.btn_save').show();
      tbl_row.find('.btn_cancel').show();

      //hide edit button
      tbl_row.find('.btn_edit').hide(); 

      //make the whole row editable
      tbl_row.find('.row_data')
      .attr('contenteditable', 'true')
      .attr('edit_type', 'button')
      .addClass('bg-warning')
      .css('padding','3px')

      //--->add the original entry > start
      tbl_row.find('.row_data').each(function(index, val) 
      {  
        //this will help in case user decided to click on cancel button
        $(this).attr('original_entry', $(this).html());
      }); 		
      //--->add the original entry > end

    });
    //--->button > edit > end

    //--->button > cancel > start	
    $(document).on('click', '.btn_cancel', function(event) 
    {
      event.preventDefault();

      var tbl_row = $(this).closest('tr');

      var row_id = tbl_row.attr('row_id');

      //hide save and cacel buttons
      tbl_row.find('.btn_save').hide();
      tbl_row.find('.btn_cancel').hide();

      //show edit button
      tbl_row.find('.btn_edit').show();

      //make the whole row editable
      tbl_row.find('.row_data')
      .attr('edit_type', 'click')	 
      .removeClass('bg-warning')
      .css('padding','') 

      tbl_row.find('.row_data').each(function(index, val) 
      {   
        $(this).html( $(this).attr('original_entry') ); 
      });  
    });
    //--->button > cancel > end

     //out put col lives
  $('.table tbody').on('click', '.btn_Sub', function () {
    var curRow = $(this).closest('tr');
    var col1 = curRow.find('td:eq(1)').text();
    console.log(col1);
  });
});






